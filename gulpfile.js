var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

/*
elixir(function(mix) {
    mix.sass(['app.scss', '../css/*.css'], 'public/css', { includePaths: ['node_modules/foundation-sites/scss']});
    mix.browserify('main.js');
    // Compile JavaScript
    mix.scripts([
        'node_modules/foundation-sites/vendor/jquery/dist/jquery.js', 
        'node_modules/foundation-sites/dist/foundation.min.js',
        'public/js/main.js'
    ], 'public/js/app.js', './'
    );
    mix.copy('resources/assets/font', 'public/font');

});
*/
elixir(function(mix) {
    mix.sass(['app.scss', '../css/*.css']);
    mix.browserify('main.js');
    // Compile JavaScript
    mix.scripts([
      //  'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        'public/js/main.js'
    ], 'public/js/app.js', './');
    mix.copy('resources/assets/font', 'public/font');

});