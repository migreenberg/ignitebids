<?php
/**
 * Profanity.php
 * Copyright (c) Epic Tech LLC, All Rights Reserved
 * @author Chris Forrence <cforrence@epictech.cm>
 *
 *
 */

namespace App\Validators;

class Profanity
{
    public function handle($attribute, $value, $parameters, $validator)
    {
        // start by replacing special characters with spaces
        $value = str_replace(['/', '_', '.', '-'], ' ', $value);
        foreach (config('validation.profanity.blacklist', []) as $bad_word => $whitelist) {
            if (strpos($value, $bad_word) !== false) {
                $it_is_bad = true;
                foreach ($whitelist as $good_word) {
                    if (strpos($value, $bad_word) === strpos($value, $good_word)) {
                        $it_is_bad = false;
                        break;
                    }
                }
                if ($it_is_bad) {
                    return false;
                }
            }
        }
        return true;
    }
}