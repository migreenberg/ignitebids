<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimal-ui"/>
    <title>Ignite Bids</title>
    <!-- Chrome, Firefox OS, Opera and Vivaldi -->
    <meta name="theme-color" content="#990000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#990000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#990000">
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <link rel="stylesheet" href="/css/app.css">
    <style>
        .iphone {
            background-color: #000000;
            height: 24px;
        }

        .spacer {
            height: 1px;
        }
    </style>
</head>

<body>
<div id="app">
    <app></app>
</div>
<div class="spacer"></div>
<script src="js/main.js"></script>
</body>
</html>
