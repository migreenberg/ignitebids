// main.js
var Vue = require('vue');
var VueResource = require('vue-resource');
Vue.use(VueResource);

import App from './components/App.vue';

Vue.filter('money', function (value) {
    value = value || 0;
    return '$' + (value / 100).toFixed(2);
});

Vue.filter('number', function (value) {
    return value.toLocaleString();
});

new Vue({
    el: '#app',
    components: {App},
    config: {
        devtools: true
    }
});