<?php
/**
 * emails.php
 * Copyright (c) Epic Tech LLC, All Rights Reserved
 * @author Chris Forrence <cforrence@epictech.cm>
 *
 *
 */
return [
    'new_customer' => [
        'subject' => 'Welcome to Ignitebids, :FirstName!'
    ]
];