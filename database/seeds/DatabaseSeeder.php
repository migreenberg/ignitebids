<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BidTypeSeeder::class);
        $this->call(AuctionTypeSeeder::class);
        $this->call(PageCategorySeeder::class);
        $this->call(PageSeeder::class);
    }
}
