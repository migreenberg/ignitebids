<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
     return [
        'username' => $faker->userName,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];

});

$factory->define(App\Models\Vendor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\Models\PromotionalCode::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});


$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->words(4, true),
        'description' => $faker->paragraph,
        'msrp' => $faker->numberBetween(500, 50000),
        'cost' => $faker->numberBetween(500, 50000),
        'shipping_cost' => $faker->numberBetween(50, 1000),
        'can_buy_it_now' => $faker->boolean,
        'sku' => $faker->uuid,
        'upc' => $faker->uuid,
        'image_url' => $faker->url,
        'vendor_id' => function () {
            if (($count = App\Models\Vendor::count('id')) === 0 || rand(1, 10) > 8) {
                return null;
            }
            return rand(1, $count);
        },
        'category_id' => function () {
            if (($count = App\Models\Category::count('id')) === 0 || rand(1, 10) > 8) {
                return null;
            }
            return rand(1, $count);
        },
    ];
});

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'label' => $faker->word,
        'background_color' => substr($faker->hexColor, 1),
        'foreground_color' => substr($faker->hexColor, 1),
        'parent_id' => function () {
            if (($count = App\Models\Category::count('id')) === 0 || rand(1, 10) > 8) {
                return null;
            }
            return rand(1, $count);
        },
    ];
});